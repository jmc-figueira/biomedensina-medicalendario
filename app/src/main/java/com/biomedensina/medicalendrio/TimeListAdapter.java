package com.biomedensina.medicalendrio;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class TimeListAdapter extends RecyclerView.Adapter<TimeListAdapter.TimeListViewHolder> {
    private List<String> intakeTimes;

    public static class TimeListViewHolder extends RecyclerView.ViewHolder{
        public TextView timeView;

        public TimeListViewHolder(TextView itemView) {
            super(itemView);
            timeView = itemView;
        }
    }

    public TimeListAdapter(List<String> data){
        intakeTimes = data;
    }

    @NonNull
    @Override
    public TimeListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        TextView v = (TextView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.intake_time_list_item, viewGroup, false);

        return new TimeListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeListViewHolder timeListViewHolder, int i) {
        timeListViewHolder.timeView.setText(intakeTimes.get(i));
    }

    @Override
    public int getItemCount() {
        return intakeTimes.size();
    }
}
