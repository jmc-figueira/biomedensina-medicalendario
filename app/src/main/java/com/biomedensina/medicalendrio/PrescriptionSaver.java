package com.biomedensina.medicalendrio;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PrescriptionSaver extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Medicalendrio_Prescriptions";
    private static final String PRESCRIPTIONS_TABLE_NAME = "prescriptions";
    private static final String PRESCRIPTIONS_TABLE_CREATE = "CREATE TABLE " + PRESCRIPTIONS_TABLE_NAME + " (" +
            "id integer PRIMARY KEY, " +
            "name text NOT NULL, " +
            "expiry_date text NOT NULL, " +
            "quantity integer NOT NULL, " +
            "image_path text" +
            ");";
    private static final String INTAKES_TABLE_NAME = "intakes";
    private static final String INTAKES_TABLE_CREATE = "CREATE TABLE " + INTAKES_TABLE_NAME + " (" +
            "id integer PRIMARY KEY, " +
            "time text NOT NULL, " +
            "prescription_id integer, " +
            "FOREIGN KEY(prescription_id) REFERENCES prescriptions(id)" +
            ");";

    public PrescriptionSaver(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PRESCRIPTIONS_TABLE_CREATE);
        db.execSQL(INTAKES_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
