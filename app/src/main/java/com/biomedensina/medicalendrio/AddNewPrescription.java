package com.biomedensina.medicalendrio;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

public class AddNewPrescription extends AppCompatActivity {

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private String currentPhotoPath;
    private List<String> intakeTimes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_prescription);
        Toolbar toolbar = (Toolbar) findViewById(R.id.add_toolbar);
        setSupportActionBar(toolbar);

        EditText dateText = findViewById(R.id.expiry_date);

        dateText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    openDatePicker(v);
                }
            }
        });

        RecyclerView intakeTimeList = findViewById(R.id.time_list);
        intakeTimes = new LinkedList<>();

        intakeTimeList.setLayoutManager(new LinearLayoutManager(this));
        intakeTimeList.setAdapter(new TimeListAdapter(intakeTimes));

    }

    public void closeActivity(View view){
        finish();
    }

    public void savePrescription(View view){
        EditText name = findViewById(R.id.prescription_name);
        EditText expiry_date = findViewById(R.id.expiry_date);
        EditText quantity = findViewById(R.id.box_quantity);

        if(TextUtils.isEmpty(name.getText())){
            name.setError("Campo necessário");
            finish();
            return;
        }
        if(TextUtils.isEmpty(expiry_date.getText())){
            expiry_date.setError("Campo necessário");
            finish();
            return;
        }
        if(TextUtils.isEmpty(quantity.getText())){
            quantity.setError("Campo necessário");
            finish();
            return;
        }

        PrescriptionSaver saver = new PrescriptionSaver(this);

        SQLiteDatabase db = saver.getWritableDatabase();

        db.beginTransaction();

        long id;

        try {
            ContentValues values = new ContentValues();
            values.put("name", name.getText().toString());
            values.put("expiry_date", expiry_date.getText().toString());
            values.put("quantity", Integer.parseInt(quantity.getText().toString()));
            if(currentPhotoPath != null)
                values.put("image_path", currentPhotoPath);

            id = db.insert("prescriptions", null, values);

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        db.beginTransaction();

        try{
            for(String time : intakeTimes){
                ContentValues intakeValues = new ContentValues();
                intakeValues.put("time", time);
                intakeValues.put("prescription_id", id);

                db.insert("intakes", null, intakeValues);

                db.setTransactionSuccessful();
            }
        } finally {
            db.endTransaction();
        }

        db.close();
        finish();
    }

    public void openDatePicker(View view){
        final EditText dateText = (EditText) view;
        Calendar currentDate = Calendar.getInstance();
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar selectedCalendar = new GregorianCalendar();
                selectedCalendar.clear();
                selectedCalendar.set(year, month, dayOfMonth);

                DateFormat selectedDate = SimpleDateFormat.getDateInstance();

                dateText.setText(selectedDate.format(selectedCalendar.getTime()));
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void openTimePicker(View view){
        Calendar currentTime = Calendar.getInstance();
        new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar selectedCalendar = new GregorianCalendar();
                selectedCalendar.clear();
                selectedCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                selectedCalendar.set(Calendar.MINUTE, minute);

                DateFormat selectedDate = SimpleDateFormat.getTimeInstance();

                RecyclerView intakeHours = findViewById(R.id.time_list);

                intakeTimes.add(selectedDate.format(selectedCalendar.getTime()));
                intakeHours.getAdapter().notifyItemInserted(intakeTimes.size() - 1);
            }
        }, currentTime.get(Calendar.HOUR_OF_DAY), currentTime.get(Calendar.MINUTE), true).show();
    }

    public void takePhoto(View view){
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePhotoIntent.resolveActivity(getPackageManager()) != null){
            File photo = null;
            try{
                photo = createImageFile();
            } catch(IOException e){
                return;
            }

            Uri photoURI = FileProvider.getUriForFile(this, "com.biomedensina.medicalendrio.fileprovider", photo);

            takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(takePhotoIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            ImageView medicationPhoto = findViewById(R.id.medication_photo);
            Bitmap imageBitmap = BitmapFactory.decodeFile(currentPhotoPath);
            medicationPhoto.setImageBitmap(imageBitmap);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

}
